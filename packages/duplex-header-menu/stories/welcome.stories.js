import { html } from 'lit-html';
import '../src/lit-header-menu';
import { logo, iconTlf, menuMobile, menu } from '../mocks/lit-header-menu-data-mock.js';

export default {
  title: 'Welcome',
};

export const Welcome = () => html`
<lit-header-menu
    .logo=${logo}
    .icontlf=${iconTlf}
    .menuMobile=${menuMobile}
    .menu=${menu}
></lit-header-menu>
`;

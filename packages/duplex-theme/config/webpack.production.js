const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpackBaseConfig = require('../../../webpack.config');
const configAssets = require('../../../configAssetsWebpack');
const CopyWebpackPlugin  = require('copy-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const distPath = 'dist';
const configScope = {
  distPath: path.resolve(distPath),
};

const assets = configAssets.copyAssets();


module.exports = () => ({
  ...webpackBaseConfig.baseConfig(configScope),
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new CopyWebpackPlugin([...assets]),
    new HTMLWebpackPlugin({
      template: path.resolve('index.html'),
      filename: 'index.html',
      minify: {
        collapseWhitespace: true,
        minifyCSS: true,
        minifyJS: true
      }
    }),
  ]
});

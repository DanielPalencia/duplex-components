const webpackBaseConfig = require('../../../webpack.config');
const portFinderSync = require('portfinder-sync');
const CopyWebpackPlugin  = require('copy-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const distPath = 'dist';
const configAssets = require('../../../configAssetsWebpack');

const configScope = {
  distPath: path.resolve(distPath)
};
const assets = configAssets.copyAssets();


module.exports = {
  ...webpackBaseConfig.baseConfig(configScope),
  mode: 'development',
  watch: true,
  plugins: [
    new CopyWebpackPlugin([...assets]),
    new HTMLWebpackPlugin({
      template: path.resolve('index.html'),
      filename: 'index.html',
      minify: {
        collapseWhitespace: true,
        minifyCSS: true,
        minifyJS: true
      }
    }),
  ],
  devServer: {
    contentBase: configScope.distPath,
    inline: true,
    host: '127.0.0.1',
    port: portFinderSync.getPort(8080),
    historyApiFallback: true,
  },
};
